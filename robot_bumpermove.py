#!/usr/bin/env python
import rospy,math
from std_msgs.msg import String
from turtlesim.msg import Pose
from geometry_msgs.msg import Twist
from kobuki_msgs.msg import BumperEvent

# Notice the topics that have been imported. Look up the descriptions
# of these messages on the ros wiki
# http://docs.ros.org/kinetic/api/turtlesim/html/msg/Pose.html
# http://docs.ros.org/api/geometry_msgs/html/msg/Twist.html

class Robot:

	def __init__(self):

		self.turtlesim_pose = Pose()
		self.turtle_bump = BumperEvent()

	def poseCallback(self,data):
			
			self.turtlesim_pose.x = data.x
			self.turtlesim_pose.y = data.y
			self.turtlesim_pose.theta = data.theta

	def bumpCallback(self,data):
		
		rospy.loginfo(data)
		self.turtle_bump.bumper = data.bumper
		self.turtle_bump.state = data.state

	def setDesiredOrientation(self,desired_angle_radians):
			print "Setting orientation to:", desired_angle_radians
			print "Current angle is:", self.turtlesim_pose.theta
			relative_angle = self.turtlesim_pose.theta - desired_angle_radians
			rotate(relative_angle, False)
			print "Current angle AFTER TURN is:",self.turtlesim_pose.theta #to check if done successfully

	def moveGoal(self,goal_pose, distance_tolerance):
			
			pub = rospy.Publisher("turtle1/cmd_vel", Twist, queue_size=10)
			rate = rospy.Rate(10)
			vel_msg = Twist()
			distance = findDistance(goal_pose.x, self.turtlesim_pose.x, goal_pose.y, self.turtlesim_pose.y)



	# Below are some functions that are generally useful
	# that you might need for this lab

	def findDistance(x1,x0,y1,y0):
			newDistance = math.sqrt((x1-x0)**2 + (y1-y0)**2)
			return newDistance


	def degrees2radians(angle):
		return angle * (math.pi/180.0)



	def move(distance, isForward):

			speed = 0.2
		
			pub = rospy.Publisher("mobile_base/commands/velocity", Twist, queue_size=10)
		
			outData = Twist()

			t0 = rospy.get_rostime().secs
		
			current_distance = 0

			rate = rospy.Rate(10)


			if(isForward == True):
			outData.linear.x = 0.2
			else:
			outData.linear.x = -0.2
		
		
			while(current_distance < distance):
			pub.publish(outData)
				t1 = rospy.get_rostime().secs
			current_distance = speed * (t1 - t0)
			if myRobot.turtle_bump.state == 1:
				current_distance = distance
			rate.sleep()
			rate.sleep()




	def rotate(relative_angle, isClockwise):

		#same as move()
		pub = rospy.Publisher("mobile_base/commands/velocity", Twist, queue_size=10)
			outData = Twist()
			t0 = rospy.get_rostime().secs

		#reference variables
			current_angle = 0
			rate = rospy.Rate(10)

		#speed set to 0.2 for the turtlebot
			speed = 0.86

			if(isClockwise == True):
			outData.angular.z = speed
			else:
			outData.angular.z = -speed

		#using the same methodoly as move()'s while loop
			while(current_angle < relative_angle):
			pub.publish(outData)
				t1 = rospy.get_rostime().secs
			current_angle = speed * (t1 - t0)
			rate.sleep()
		

		

	def moveRobot():

			rospy.init_node('robotMover', anonymous=True)
			rospy.Subscriber("/turtle1/pose", Pose, myRobot.poseCallback)
			rospy.Subscriber("/mobile_base/events/bumper", BumperEvent, myRobot.bumpCallback)
			rate = rospy.Rate(10)
		

		# The following loop creates a never-ending loop moving forward until you bump into something, turning, then continuing to move

			notFinished = True
			while notFinished == True:
				if myRobot.turtle_bump.state == 0:
					move(100, True)
						rate.sleep()
				if myRobot.turtle_bump.state == 1:
						move(1, False)
						rate.sleep()
			if myRobot.turtle_bump.state == 0:
				rotate(degrees2radians(70.0),False)
				rate.sleep()
			else:
				rotate(degrees2radians(70.0),True)

		#rotate(degrees2radians(64.0),False)
		#rate.sleep()
	 
		#move(3.0, True)
		#rate.sleep()


		#myRobot.setDesiredOrientation(degrees2radians(-270))
		#rate.sleep()
		#runs correctly

		#was not able to get test (4) completely working
		#(4) goal_pose = Pose()
		#(4) goal_pose.x = 1.0
		#(4) goal_pose.y = 1.0
		#(4) goal_pose.theta = 0.0
		#(4) myRobot.moveGoal(goal_pose,0.1)
		#(4) rate.sleep()


			rospy.spin()

if __name__ == '__main__':
		myRobot = Robot()
		moveRobot()
